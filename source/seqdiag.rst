seqdiag
=======

.. seqdiag::

  seqdiag {
    seqdiag -> "sequence-diagrams" [label = "generates"];
    seqdiag --> "is very easy!";
  }


.. note::

  Seqdiag Official Dcument is `here <http://blockdiag.com/en/seqdiag/>`_