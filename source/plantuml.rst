plantuml
========

PlantUML is a component that allows to quickly write :

- Sequence diagram
- Usecase diagram
- Class diagram
- Activity diagram, (here is the new syntax)
- Component diagram
- State diagram
- Deployment diagram
- Object diagram

.. note::
  See `Official Site <http://plantuml.com/>`_

sequence diagram
----------------

.. uml::

  @startuml
  participant User

  User -> A: DoWork
  activate A #FFBBBB

  A -> A: Internal call
  activate A #DarkSalmon

  A -> B: << createRequest >>
  activate B

  B --> A: RequestCreated
  deactivate B
  deactivate A
  A -> User: Done
  deactivate A

  @enduml

usecase diagram
---------------

.. uml::

  @startuml
  left to right direction
  skinparam packageStyle rect
  actor customer
  actor clerk
  rectangle checkout {
    customer -- (checkout)
    (checkout) .> (payment) : include
    (help) .> (checkout) : extends
    (checkout) -- clerk
  }
  @enduml

class digram
------------

.. uml::

  @startuml

  class BaseClass

  namespace net.dummy #DDDDDD {
      .BaseClass <|-- Person
      Meeting o-- Person

      .BaseClass <|- Meeting
  }

  namespace net.foo {
    net.dummy.Person  <|- Person
    .BaseClass <|-- Person

    net.dummy.Meeting o-- Person
  }

  BaseClass <|-- net.unused.Person

  @enduml


activity diagram
----------------

.. uml::

  @startuml

  start
  :ClickServlet.handleRequest();
  :new page;
  if (Page.onSecurityCheck) then (true)
    :Page.onInit();
    if (isForward?) then (no)
      :Process controls;
      if (continue processing?) then (no)
        stop
      endif

      if (isPost?) then (yes)
        :Page.onPost();
      else (no)
        :Page.onGet();
      endif
      :Page.onRender();
    endif
  else (false)
  endif

  if (do redirect?) then (yes)
    :redirect process;
  else
    if (do forward?) then (yes)
      :Forward request;
    else (no)
      :Render page template;
    endif
  endif

  stop

  @enduml


component diagram
-----------------

.. uml::

  @startuml

  package "Some Group" {
    HTTP - [First Component]
    [Another Component]
  }

  node "Other Groups" {
    FTP - [Second Component]
    [First Component] --> FTP
  }

  cloud {
    [Example 1]
  }


  database "MySql" {
    folder "This is my folder" {
      [Folder 3]
    }
    frame "Foo" {
      [Frame 4]
    }
  }


  [Another Component] --> [Example 1]
  [Example 1] --> [Folder 3]
  [Folder 3] --> [Frame 4]

  @enduml

state diagram
-------------

.. uml::

  @startuml
  scale 350 width
  [*] --> NotShooting

  state NotShooting {
    [*] --> Idle
    Idle --> Configuring : EvConfig
    Configuring --> Idle : EvConfig
  }

  state Configuring {
    [*] --> NewValueSelection
    NewValueSelection --> NewValuePreview : EvNewValue
    NewValuePreview --> NewValueSelection : EvNewValueRejected
    NewValuePreview --> NewValueSelection : EvNewValueSaved

    state NewValuePreview {
       State1 -> State2
    }

  }
  @enduml


object diagram
--------------

.. uml::

  @startuml
  object Object01 {
    company_cd = "BZ1"
    user_cd = "U11110"
    locale_id = "en"
  }
  object Object02
  object Object03
  object Object04
  object Object05
  object Object06
  object Object07
  object Object08

  Object01 <|-- Object02
  Object03 *-- Object04
  Object05 o-- "4" Object06
  Object07 .. Object08 : some labels
  @enduml