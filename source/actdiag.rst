actdiag
=======

.. actdiag::

  actdiag {
   write -> convert -> image

   lane user {
      label = "User"
      write [label = "Writing reST"];
      image [label = "Get diagram IMAGE"];
   }
   lane actdiag {
      convert [label = "Convert reST to Image"];
   }
  }


.. note::

  Actdiag Official Dcument is `here <http://blockdiag.com/en/actdiag/>`_