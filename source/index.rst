===================
Sphinx Docker image
===================

About sphinx-plantuml Docker image
===================================

include
-------

- sphinx
- plantuml
- blockdiag series
- bootstrap theme

you need only sphinx-source-files and build script.

How to use Sphinx Docker image
===============================

How to use Sphinx Docker image in gitlab-ci.yml

gitlab-ci.yml

.. code-block:: yaml

  image: tsgkadot/sphinx-plantuml

  stages:
    - build

  pages:
    stage: build
    script:
      - pip install -r requirements.txt -U
      - sphinx-build -b html ./source public
    artifacts:
      paths:
        - public
    tags:
      - docker


.. note::

  - tsgkadot/sphinx-plantuml images : see `Doccker Hub Repository <https://hub.docker.com/r/tsgkadot/sphinx-plantuml/>`_
  - gitlab-ci.yml and other files : see  `Gitlab Repository <https://gitlab.com/tsgkdt/sphinx-plantuml>`_

Demos
=====

.. toctree::
  :maxdepth: 1

  plantuml
  blockdiag
  actdiag
  seqdiag
  nwdiag

